import pandas as pd

каталог="/home/bolotsky/moe/shariki/shariki1/заготовки ресурсов/data/"

def ReadDataLevel(filedir, numlevel):
    filpole='Pole%02d.txt' % numlevel
    fil=open(filedir+'/'+filpole)
    txtfil=fil.readlines()
    fil.close()
    try:
        verts=eval(txtfil[0])
        if type(verts)!=type([]):
            raise
        #if len(verts)<2:
        #    raise
        razryvy=eval(txtfil[1])
        if type(razryvy)!=type([]):
            raise
        unitazy=eval(txtfil[2])
        if type(unitazy)!=type([]):
            raise
    except:
        print("Хрень случилась")
        return
    return verts,razryvy,unitazy


def построение_файла(вершины, разрывы, унитазы):
    результат = ""

    результат += "количество_путей=%d\n\n" % len(вершины)

    for номер_пути, путь in enumerate(вершины):
        df = pd.DataFrame(путь)
        данные_csv = df.to_csv(header=False, index=False)
        результат += "вершины %d\n" % номер_пути
        результат += данные_csv
        результат += "\n"

    for номер_пути, разрывы_пути in enumerate(разрывы):
        df = pd.DataFrame(разрывы_пути)
        данные_csv = df.to_csv(header=False, index=False)
        результат += "разрывы %d\n" % номер_пути
        результат += данные_csv
        результат += "\n"

    df = pd.DataFrame(унитазы)
    данные_csv = df.to_csv(header=False, index=False)
    результат += "унитазы\n"
    результат += данные_csv
    результат += "\n"

    return результат


def конвертация(каталог):
    for номер_поля in range(1,13):
        вершины, разрывы, унитазы = ReadDataLevel(каталог, номер_поля)
        текст = построение_файла(вершины, разрывы, унитазы)
        open(каталог+("поле%02d.txt" % номер_поля), "w", encoding="utf8").write(текст)
