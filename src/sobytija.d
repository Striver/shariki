module sobytija;

import ddd.zavisimost.sobytija_SDL2;

class СобытияИгры: МинимальныйОбработчикСобытий {
    private bool принудительный_выход;
    private bool флаг_управление_мышью;

    this() {
        super();
        принудительный_выход = false;
        флаг_управление_мышью = false;
    }
    
    override bool обработка_всех_событий() {
        SDL_Event e;
        if (принудительный_выход)
            return true;
		bool результат = false; 
        while (SDL_PollEvent(&e))
            результат = результат || функция1.get(e.type, &заглушка)(e);
        return результат; // Если истина - выходим из программы
    }

    void принудительно_выйти() {
        принудительный_выход = true;
    }

    void включить_управление_мышью() {
        флаг_управление_мышью = true;
    }

    void выключить_управление_мышью() {
        флаг_управление_мышью = false;
    }
    
    override public @property bool управление_мышью() {
        return флаг_управление_мышью;
    }
}

СобытияИгры события_игры;
    
